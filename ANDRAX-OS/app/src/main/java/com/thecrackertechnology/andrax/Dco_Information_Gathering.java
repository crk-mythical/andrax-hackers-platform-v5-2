package com.thecrackertechnology.andrax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.thecrackertechnology.dragonterminal.bridge.Bridge;
import com.thecrackertechnology.dragonterminal.bridge.SessionId;

public class Dco_Information_Gathering extends Activity {

    int REQUEST_CODE_RUN = 1;
    private SessionId lastSessionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);


        setContentView(R.layout.dco_information_gathering);

        CardView cardviewhping = findViewById(R.id.card_view_hping3);
        CardView cardviewnping = findViewById(R.id.card_view_nping);
        CardView cardviewwhois = findViewById(R.id.card_view_whois);
        CardView cardviewlbd = findViewById(R.id.card_view_lbd);
        CardView cardviewdig = findViewById(R.id.card_view_dig);
        CardView cardviewkatanads = findViewById(R.id.card_view_katanads);
        CardView cardviewatscan = findViewById(R.id.card_view_atscan);
        CardView cardviewbingip2hosts = findViewById(R.id.card_view_bingip2hosts);
        CardView cardviewcloudfail = findViewById(R.id.card_view_cloudfail);
        CardView cardviewcloudmare = findViewById(R.id.card_view_cloudmare);
        CardView cardviewdnsrecon = findViewById(R.id.card_view_dnsrecon);
        CardView cardviewraccoon = findViewById(R.id.card_view_raccoon);
        CardView cardviewpwnedornot = findViewById(R.id.card_view_pwnedornot);
        CardView cardviewsmtpuserenum = findViewById(R.id.card_view_smtpuserenum);
        CardView cardviewismtp = findViewById(R.id.card_view_ismtp);
        CardView cardviewfirewalk = findViewById(R.id.card_view_firewalk);
        CardView cardviewbraa = findViewById(R.id.card_view_braa);
        CardView cardview0trace = findViewById(R.id.card_view_0trace);
        CardView cardviewfierce = findViewById(R.id.card_view_fierce);
        CardView cardviewdmitry = findViewById(R.id.card_view_dmitry);
        CardView cardviewintrace = findViewById(R.id.card_view_intrace);
        CardView cardviewstheharvester = findViewById(R.id.card_view_theharvester);
        CardView cardviewbuster = findViewById(R.id.card_view_buster);
        CardView cardviewsublist3r = findViewById(R.id.card_view_sublist3r);
        CardView cardviewshuffledns = findViewById(R.id.card_view_shuffledns);
        CardView cardviewmassdns = findViewById(R.id.card_view_massdns);
        CardView cardviewchameleon = findViewById(R.id.card_view_chameleon);
        CardView cardviewdnsmap = findViewById(R.id.card_view_dnsmap);
        CardView cardviewvault = findViewById(R.id.card_view_vault);
        CardView cardviewxray = findViewById(R.id.card_view_xray);
        CardView cardviewtldscanner = findViewById(R.id.card_view_tld_scanner);
        CardView cardviewamass = findViewById(R.id.card_view_amass);
        CardView cardviewmaryam = findViewById(R.id.card_view_maryam);
        CardView cardviewmaltego = findViewById(R.id.card_view_maltego);
        CardView cardviewredhawk = findViewById(R.id.card_view_redhawk);
        CardView cardviewonesixtyone = findViewById(R.id.card_view_onesixtyone);
        CardView cardviewarping = findViewById(R.id.card_view_arping);
        CardView cardviewdnsdict6 = findViewById(R.id.card_view_dnsdict6);
        CardView cardviewdnsenum = findViewById(R.id.card_view_dnsenum);
        CardView cardviewinverselookup6 = findViewById(R.id.card_view_inverselookup6);
        CardView cardviewthcping6 = findViewById(R.id.card_view_thcping6);
        CardView cardviewtrace6 = findViewById(R.id.card_view_trace6);
        CardView cardviewnetdiscover = findViewById(R.id.card_view_netdiscover);
        CardView cardviewsnmpwn = findViewById(R.id.card_view_snmpwn);


        cardviewhping.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hping3 --help");

            }
        });

        cardviewnping.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("nping");

            }
        });

        cardviewwhois.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("whois");

            }
        });

        cardviewlbd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("lbd");

            }
        });

        cardviewdig.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dig -h");

            }
        });

        cardviewkatanads.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("kds -h");

            }
        });

        cardviewatscan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("atscan -h");

            }
        });

        cardviewbingip2hosts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("bing-ip2hosts");

            }
        });

        cardviewcloudfail.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cloudfail -h");

            }
        });

        cardviewcloudmare.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cloudmare -h");

            }
        });

        cardviewdnsrecon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dnsrecon");

            }
        });

        cardviewraccoon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("raccoon --help");

            }
        });

        cardviewpwnedornot.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("pwnedornot");

            }
        });

        cardviewsmtpuserenum.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("smtp-user-enum");

            }
        });

        cardviewismtp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("iSMTP");

            }
        });

        cardviewonesixtyone.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("onesixtyone");

            }
        });

        cardviewfirewalk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("firewalk");

            }
        });

        cardview0trace.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("0trace");

            }
        });

        cardviewbraa.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("braa");

            }
        });

        cardviewfierce.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("fierce --help");

            }
        });

        cardviewdmitry.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dmitry");

            }
        });

        cardviewintrace.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("intrace");

            }
        });

        cardviewstheharvester.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("theHarvester");

            }
        });

        cardviewbuster.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("buster");

            }
        });

        cardviewchameleon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("chameleon -h");

            }
        });

        cardviewdnsmap.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dnsmap");

            }
        });

        cardviewvault.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo vault");

            }
        });


        /**
         *
         * Help me, i'm dying...
         *
         **/

        cardviewxray.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xray");

            }
        });

        cardviewtldscanner.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("tld_scanner");

            }
        });

        cardviewamass.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("amass");

            }
        });

        cardviewmaryam.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("maryam");

            }
        });

        cardviewmaltego.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("maltego");

            }
        });

        cardviewsublist3r.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sublist3r");

            }
        });

        cardviewshuffledns.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("shuffledns");

            }
        });

        cardviewmassdns.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("massdns");

            }
        });

        cardviewredhawk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("rhawk");

            }
        });

        cardviewarping.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("arping");

            }
        });

        cardviewdnsdict6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dnsdict6");

            }
        });

        cardviewdnsenum.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dnsenum");

            }
        });

        cardviewinverselookup6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("inverse_lookup6");

            }
        });

        cardviewthcping6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("thcping6");

            }
        });

        cardviewtrace6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("trace6");

            }
        });

        cardviewnetdiscover.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo netdiscover");

            }
        });

        cardviewsnmpwn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("snmpwn --help");

            }
        });


    }

    public void run_hack_cmd(String cmd) {

        Intent intent = Bridge.createExecuteIntent(cmd);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);

    }

    @Override
    public void onPause() {

        super.onPause();
        finish();
    }

}
