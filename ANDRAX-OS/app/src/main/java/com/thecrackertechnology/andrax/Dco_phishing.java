package com.thecrackertechnology.andrax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;

import com.thecrackertechnology.dragonterminal.bridge.Bridge;

public class Dco_phishing extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);


        setContentView(R.layout.dco_phishing);

        CardView cardviewshellphish = findViewById(R.id.card_view_shellphish);
        CardView cardviewgophish = findViewById(R.id.card_view_gophish);
        CardView cardviewevilginx2 = findViewById(R.id.card_view_evilginx2);
        CardView cardviewmodlishka = findViewById(R.id.card_view_modlishka);
        CardView cardviewurlcrazy = findViewById(R.id.card_view_urlcrazy);
        CardView cardviewdnstwist = findViewById(R.id.card_view_dnstwist);


        cardviewshellphish.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo shellphish");

            }
        });

        cardviewgophish.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo gophish");

            }
        });

        cardviewevilginx2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo evilginx2");

            }
        });

        cardviewmodlishka.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo modlishka");

            }
        });

        cardviewurlcrazy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("urlcrazy");

            }
        });

        cardviewdnstwist.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dnstwist");

            }
        });

    }

    public void run_hack_cmd(String cmd) {

        Intent intent = Bridge.createExecuteIntent(cmd);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);

    }

    @Override
    public void onPause() {

        super.onPause();
        finish();
    }

}
